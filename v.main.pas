unit v.main;

interface

uses
  mDevice.Windows,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RzButton, System.ImageList, Vcl.ImgList, RzPanel, Vcl.ExtCtrls, RzSplit,
  Vcl.StdCtrls, Vcl.Grids, Vcl.ValEdit,
  System.Generics.Collections, Vcl.ComCtrls, Vcl.Menus, RzStatus, RzForms, System.Actions, Vcl.ActnList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnMan
  ;

type
  TvMain = class(TForm)
    RzStatusBar1: TRzStatusBar;
    RzToolbar1: TRzToolbar;
    ImageList: TImageList;
    RzSplitter: TRzSplitter;
    PropDev: TValueListEditor;
    TreeDev: TTreeView;
    RzSpacer1: TRzSpacer;
    RzSpacer2: TRzSpacer;
    ButtonRefresh: TRzToolButton;
    ButtonExpandAll: TRzToolButton;
    ButtonCallapseAll: TRzToolButton;
    ButtonCopy: TRzToolButton;
    RzToolButton1: TRzToolButton;
    PopupCopyFmt: TPopupMenu;
    CopyasKeyValueformat1: TMenuItem;
    SepratedbyComma1: TMenuItem;
    SeperatedbyTab1: TMenuItem;
    RzVersionInfoStatus1: TRzVersionInfoStatus;
    RzVersionInfo: TRzVersionInfo;
    RzFormState1: TRzFormState;
    ActionManager: TActionManager;
    ActionCopyPropRows: TAction;
    ActionExpandAll: TAction;
    ActionCollapseAll: TAction;
    ActionRefresh: TAction;
    ActionSelectAllProp: TAction;
    procedure FormCreate(Sender: TObject);

    procedure FormDestroy(Sender: TObject);
    procedure TreeDevClick(Sender: TObject);
    procedure PopupCopyFmtClick(Sender: TObject);
    procedure ActionCopyPropRowsExecute(Sender: TObject);
    procedure ActionExpandAllExecute(Sender: TObject);
    procedure ActionCollapseAllExecute(Sender: TObject);
    procedure ActionRefreshExecute(Sender: TObject);
    procedure ActionSelectAllPropExecute(Sender: TObject);
  private
    FDic: TObjectDictionary<String, TObjectList<TWinDevice>>;
    function BuildWinDevList(var AErCode: Cardinal; var AErMsg: String): Boolean;
    procedure UpdateTreeDev;
  public
  end;

var
  vMain: TvMain;

implementation

{$R *.dfm}

uses
  svc.option,

  SetupAPI, Hid, Clipbrd
  ;

procedure TvMain.ActionCollapseAllExecute(Sender: TObject);
begin
  TreeDev.Items.BeginUpdate;
  TreeDev.FullCollapse;
  TreeDev.Items.EndUpdate;
end;

procedure TvMain.ActionCopyPropRowsExecute(Sender: TObject);
const
  NKeyValue = 0;
  NComma = 1;
  NTab = 2;
  SDelimeter: array[1..2] of String = (',', #9);
var
  LBuf: TStringList;
  LFmt, r: Integer;
begin
  if PropDev.Strings.Count = 0 then
    Exit;

  LBuf := TStringList.Create;
  try
    LFmt := svcOption.CopyFmt;
    with PropDev do
      for r := Selection.Top to Selection.Bottom do
        case LFmt of
          NKeyValue: LBuf.Add(Strings[r -1]);
          NComma,
          NTab  :  LBuf.Add(Strings.Names[r -1] + SDelimeter[LFmt] + Strings.ValueFromIndex[r -1]);
        end;
    Clipboard.AsText := LBuf.Text;
  finally
    FreeAndNil(LBuf)
  end;
end;

procedure TvMain.ActionExpandAllExecute(Sender: TObject);
begin
  TreeDev.Items.BeginUpdate;
  TreeDev.FullExpand;
  TreeDev.Items.EndUpdate;
end;

procedure TvMain.ActionRefreshExecute(Sender: TObject);
var
  LErCode: Cardinal;
  LErMsg: String;
begin
  if not BuildWinDevList(LErCode, LErMsg) then
    raise EOSError.CreateFmt('[%d]%s', [LErCode, LErMsg]);

  UpdateTreeDev;
end;

procedure TvMain.ActionSelectAllPropExecute(Sender: TObject);
var
  LCnt: Integer;
begin
  LCnt := PropDev.Strings.Count;
  if LCnt = 0 then
    Exit;

  PropDev.Selection := TGridRect(Rect(0, 1, 1, LCnt));
end;

function TvMain.BuildWinDevList(var AErCode: Cardinal; var AErMsg: String): Boolean;
var
  hDevInf: HDEVINFO;
  LDevInfoData: TSPDevInfoData;
  i: DWORD;
  LDev: TWinDevice;
  LError: Cardinal;
  LDevClassDesc: String;
begin
  hDevInf := SetupDiGetClassDevs(nil, {Enumerator}nil, 0, {DIGCF_DEVICEINTERFACE or DIGCF_PRESENT or} DIGCF_ALLCLASSES);
  if Cardinal(hDevInf) = INVALID_HANDLE_VALUE then
  begin
    AErCode := GetLastError;
    AErMsg := SysErrorMessage(AErCode);
    Exit(False);
  end;

  LDevInfoData := Default(TSPDevInfoData);
  LDevInfoData.cbSize := Sizeof(TSPDevInfoData);
  i := 0;
  while SetupDiEnumDeviceInfo(hDevInf, i, LDevInfoData) do
  begin
    Inc(i);
    LDev := TWinDevice.Create(hDevInf, LDevInfoData);
    LDevClassDesc := LDev.DeviceClassDescription;
    if LDevClassDesc.IsEmpty then
      Continue;

    if not FDic.ContainsKey(LDevClassDesc) then
      FDic.Add(LDevClassDesc, TObjectList<TWinDevice>.Create(True));
    FDic[LDevClassDesc].Add(LDev);
  end;

  LError := GetLastError;
  if (LError <> NO_ERROR) and (LError <> ERROR_NO_MORE_ITEMS) then
  begin
    AErCode := LError;
    AErMsg := SysErrorMessage(LError);
    Exit(False);
  end;
  Result := True;
end;

procedure TvMain.UpdateTreeDev;
var
  LDevClass: string;
  LDevClassNode: TTreeNode;
  LDevList: System.Generics.Collections.TObjectList<TWinDevice>;
  i: Integer;
  LDev: TWinDevice;
  LDevName: string;
begin
  TreeDev.Items.BeginUpdate;
  try
    TreeDev.Items.Clear;
    for LDevClass in FDic.Keys do
    begin
      LDevClassNode := TreeDev.Items.Add(nil, LDevClass);
      LDevList := FDic[LDevClass];
      for i := 0 to LDevList.Count - 1 do
      begin
        LDev := LDevList[i];
        LDevName := LDev.FriendlyName;
        if LDevName.IsEmpty then
          LDevName := LDev.Description;
        TreeDev.Items.AddChildObject(LDevClassNode, LDevName, Pointer(LDev));
      end;
    end;
  finally
    TreeDev.Items.EndUpdate;
  end;
  ActionExpandAll.Enabled := TreeDev.Items.Count > 0;
  ActionCollapseAll.Enabled := TreeDev.Items.Count > 0;
  ActionCopyPropRows.Enabled := TreeDev.Items.Count > 0;
end;

procedure TvMain.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  LoadHid;
  LoadSetupApi;
  ButtonRefresh.Enabled := IsSetupApiLoaded;
  FDic := TObjectDictionary<String, TObjectList<TWinDevice>>.Create;

  RzSplitter.Position := svcOption.SplitPos;
  for i := 0 to PopupCopyFmt.Items.Count -1 do
    if PopupCopyFmt.Items[i].Tag = svcOption.CopyFmt then
    begin
      PopupCopyFmt.Items[i].Click;
      Break;
    end;
end;

procedure TvMain.FormDestroy(Sender: TObject);
begin
  svcOption.SplitPos := RzSplitter.Position;
  FreeAndNil(FDic);
end;

procedure TvMain.PopupCopyFmtClick(Sender: TObject);
const
  SFmts: TArray<String> = ['Key=Value', 'Comma', 'Tab'];
var
  LMenu: TMenuItem absolute Sender;
begin
  if LMenu.Checked then
    Exit;

  LMenu.Checked := True;
  svcOption.CopyFmt := LMenu.Tag;
  ActionCopyPropRows.Caption := 'Copy selected rows as ' + SFmts[LMenu.Tag];
end;

procedure TvMain.TreeDevClick(Sender: TObject);
var
  LNode: TTreeNode;
  LDev: TWinDevice;
begin
  PropDev.Strings.BeginUpdate;
  try
    PropDev.Strings.Clear;
    LNode := TreeDev.Selected;
    if not Assigned(LNode) or not Assigned(LNode.Parent) then
      Exit;

    LDev := TWinDevice(LNode.Data);
    PropDev.InsertRow('Capabilities', LDev.Capabilities, True);
    PropDev.InsertRow('Characteristics', LDev.Characteristics, True);
    PropDev.InsertRow('ConfigFlags', LDev.ConfigFlags, True);
    PropDev.InsertRow('DeviceClassDescription', LDev.DeviceClassDescription, True);
    PropDev.InsertRow('InstallState', LDev.InstallState, True);
    PropDev.InsertRow('PowerData', LDev.PowerData, True);
    PropDev.InsertRow('LegacyBusType', LDev.LegacyBusType, True);
    PropDev.InsertRow('Address', LDev.Address.ToHexString, True);
    PropDev.InsertRow('BusTypeGUID', LDev.BusTypeGUID.ToString, True);
    PropDev.InsertRow('BusNumber', LDev.BusNumber.ToHexString, True);
    PropDev.InsertRow('ClassGUID', LDev.ClassGUID.ToString, True);
    PropDev.InsertRow('CompatibleIDS', LDev.CompatibleIDS, True);
    PropDev.InsertRow('DeviceClassName', LDev.DeviceClassName, True);
    PropDev.InsertRow('DriverName', LDev.DriverName, True);
    PropDev.InsertRow('Description', LDev.Description, True);
    PropDev.InsertRow('Enumerator', LDev.Enumerator, True);
    PropDev.InsertRow('FriendlyName', LDev.FriendlyName, True);
    PropDev.InsertRow('HardwareID', LDev.HardwareID, True);
    PropDev.InsertRow('Service', LDev.Service, True);
    PropDev.InsertRow('Location', LDev.Location , True);
    PropDev.InsertRow('LowerFilters', LDev.LowerFilters, True);
    PropDev.InsertRow('Manufacturer', LDev.Manufacturer, True);
    PropDev.InsertRow('PhisicalDriverName', LDev.PhisicalDriverName, True);
    PropDev.InsertRow('RemovalPolicy', LDev.RemovalPolicy, True);
    PropDev.InsertRow('RemovalPolicyHWDefault', LDev.RemovalPolicyHWDefault, True);
    PropDev.InsertRow('RemovalPolicyOverride', LDev.RemovalPolicyOverride, True);
    PropDev.InsertRow('UINumber', LDev.UINumber.ToHexString, True);
    PropDev.InsertRow('UINumberDecription', LDev.UINumberDecription, True);
    PropDev.InsertRow('UpperFilters', LDev.UpperFilters, True);
  finally
    PropDev.Strings.EndUpdate;
  end;
  PropDev.Selection := TGridRect(TRect.Create(1,1,0,1));
end;

end.
