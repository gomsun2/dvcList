unit svc.option;

interface

uses
  mRegOption,
  System.SysUtils, System.Classes, RzCommon;

type
  TDataModule = TRegOption;
  TsvcOption = class(TDataModule)
    Reg: TRzRegIniFile;
    procedure DataModuleCreate(Sender: TObject);
  private
  protected
    function GetRegIniFile(var APath: String): TRzRegIniFile; override;
  public
    property CopyFmt: Integer index $0000 read GetInteger write SetInteger;
    property SplitPos: Integer index $0001 read GetInteger write SetInteger;
  end;

var
  svcOption: TsvcOption;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDataModule1 }

procedure TsvcOption.DataModuleCreate(Sender: TObject);
begin
  Add('Common', [
    ['CopuFmt', '0'],
    ['SplitPos', '200']
  ]);
end;

function TsvcOption.GetRegIniFile(var APath: String): TRzRegIniFile;
begin
  Result := Reg;
  APath := 'gs2\dvclist';
end;

end.
